const express = require("express");

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const Usuario = require("../models/usuario");

const app = express();
app.set("views", "./src/views"); // indicando donde está la carpeta view

// RUTA PARA IR A LA VISTA LOGIN:
app.get("/login", (req, res) => {
    res.render("login");
});

// RUTA PARA GENERAR TOKEN
app.post("/login", (req, res) => {
    let body = req.body;
    Usuario.findOne({ email: body.email }, (err, usuarioDB) => {
        if (err) {
            return res.render('login',{msg: 'Hubo un error en la petición.'})
        }
        
        if (!usuarioDB) {
            return res.render('login',{msg: 'Credenciales inválidas'})
        }

        if (!bcrypt.compareSync(body.password, usuarioDB.password)) {
            res.render('login',{msg: 'Credenciales inválidas'})
        }

        let token = jwt.sign(
            {
                usuario: usuarioDB,
            },
            process.env.SEED,
            {
                expiresIn: process.env.CADUCIDAD_TOKEN,
            }
        );

        Usuario.find({}).exec((err, usuarios) => {
            if (err) {
                return res.status(400).json({
                    ok: false,
                    err,
                });
            }
            res.render("index", {
                usuarios: usuarios,
                token: token,
            });
        });

        // res.redirect('/usuario')
        // res.json({
        // ok: true,
        // usuario: usuarioDB,
        // token
        // });
    });
});

module.exports = app;
