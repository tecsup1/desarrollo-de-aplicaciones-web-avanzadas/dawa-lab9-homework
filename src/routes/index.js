
const express = require("express");

const app = express();
app.set('views', './src/views'); // indicando donde está la carpeta view

app.use(require("./usuario"));
app.use(require("./login"));

module.exports = app