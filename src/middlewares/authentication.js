const jwt = require('jsonwebtoken');

// Verificar token:
let verificaToken = (req,res,next)=>{
    // let token = req.get('token');
    let token = req.body.input_token;
    console.log("dentro de authenticacion: ",token)
    jwt.verify(token, process.env.SEED, (err, decoded)=>{
        console.log("alert: ",err)
        if(err){
            return res.render('error',{err:err});
        }
 
        next();
    });
};

module.exports = {
    verificaToken,
}